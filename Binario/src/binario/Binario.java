package binario;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.Color;

public class Binario{
  public static void main(String args[])throws IOException{
    BufferedImage Ledger = null;
    File f = null;
    int umbral=100;
    try{
      f = new File("joker.jpg");
      Ledger = ImageIO.read(f);
    }catch(IOException e){
      System.out.println(e);
    }

    int width = Ledger.getWidth();
    int height = Ledger.getHeight();
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int p = Ledger.getRGB(x,y);

        int a = (p>>24)&0xff;
        int r = (p>>16)&0xff;
        int g = (p>>8)&0xff;
        int b = p&0xff;
        int NuevoRed;
        int NuevoGreen;
        int NuevoBlue;

        int promedio = (r+g+b)/3;
        if(promedio<umbral){
            NuevoRed=0;
            NuevoGreen=0;
            NuevoBlue=0;
        }
        else{
            NuevoRed=255;
            NuevoGreen=255;
            NuevoBlue=255;
        }
        p = (a<<24) | (NuevoRed<<16) | (NuevoGreen<<8) | NuevoBlue;

        Ledger.setRGB(x, y, p);
      }
    }
    try{
      f = new File("./binario.jpg");
      ImageIO.write(Ledger, "jpg", f);
    }catch(IOException e){
      System.out.println(e);
    }
  }
}