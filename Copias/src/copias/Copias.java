package copias;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
public class Copias{
    public boolean copyFile(String fromFile, String toFile) {
        File origen = new File(fromFile);
        File destino = new File(toFile);
        if (origen.exists()) {
            try {
                InputStream in = new FileInputStream(origen);
                OutputStream out = new FileOutputStream(destino);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }
    public static void main(String args[]) {
        Copias javaIOUtils = new Copias();
        String fromFile = "./ipncolor.jpg";
        String toFile = "./Copia.jpg";
        boolean result = javaIOUtils.copyFile(fromFile, toFile);
        System.out.println(result?
            "Imagen copiada con nombre Copia.jpg":
                "Error en el copiado");

    }
}
