package rojoazul;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
public class RojoAzul{
  public static void main(String args[])throws IOException{
    BufferedImage fordmustang = null;
    File f = null;
    try{
      f = new File("./mustang.jpg");
      fordmustang = ImageIO.read(f);
    }catch(IOException e){
      System.out.println(e);
    }
    int width = fordmustang.getWidth();
    int height = fordmustang.getHeight();
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int p = fordmustang.getRGB(x,y);

        int a = (p>>24)&0xff;
        int r = (p>>16)&0xff;
        int b = p&0xff;
        
        p = (a<<24) | (r<<16) | 0 | b;
        fordmustang.setRGB(x, y, p);
      }
    }
    try{
      f = new File("./RojiAzul.jpg");
      ImageIO.write(fordmustang, "jpg", f);
    }catch(IOException e){
      System.out.println(e);
    }
  }
}