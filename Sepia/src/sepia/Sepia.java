package sepia;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
public class Sepia{
  public static void main(String args[])throws IOException{
    BufferedImage color = null;
    File f = null;
    try{
      f = new File("./tiger.jpg");
      color = ImageIO.read(f);
    }catch(IOException e){
      System.out.println(e);
    }
    int width = color.getWidth();
    int height = color.getHeight();
    for(int y = 0; y < height; y++){
      for(int x = 0; x < width; x++){
        int p = color.getRGB(x,y);
        int a = (p>>24)&0xff;
        int r = (p>>16)&0xff;
        int g = (p>>8)&0xff;
        int b = p&0xff;
        int NuevoRed = (int)(0.393*r + 0.769*g + 0.189*b);
        int NuevoGreen = (int)(0.349*r + 0.686*g + 0.168*b);
        int NuevoBlue = (int)(0.272*r + 0.534*g + 0.131*b);

        if(NuevoRed > 255){
          r = 255;
        }else{
          r = NuevoRed;
        }

        if(NuevoGreen > 255){
          g = 255;
        }else{
          g = NuevoGreen;
        }

        if(NuevoBlue > 255){
          b = 255;
        }else{
          b = NuevoBlue;
        }
        p = (a<<24) | (r<<16) | (g<<8) | b;
        color.setRGB(x, y, p);
      }
    }
    try{
      f = new File("./Sepia.jpg");
      ImageIO.write(color, "jpg", f);
    }catch(IOException e){
      System.out.println(e);
    }
  }
}